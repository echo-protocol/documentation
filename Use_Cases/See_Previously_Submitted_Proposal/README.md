## USE CASE: SEE A PREVIOUSLY SUBMITTED PROPOSAL

***Actors:***

* User

***Scenario:***

* User authenticates and gets to the home screen (see [home_screen.png](../home_screen.png))

* User clicks the “See submitted proposals” button and accesses the “Your submitted proposals” screen, where titles and upvotes counts are displayed for the proposals previously submitted by the user (see [submitted_proposals.png](./submitted_proposals.png))

* User clicks on the proposal of his/her choice and accesses the proposals’s details: proposal id number, title, description, choices, status (proposal submitted, vote deployed or proposal discarded) and upvotes count (see [proposal_details.png](./proposal_details.png))
