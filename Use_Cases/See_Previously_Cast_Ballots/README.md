## USE CASE: SEE A PREVIOUSLY CAST BALLOT (AND REPLACE IT, OR SEE VOTE RESULTS)

***Actors:***

* User

***Scenario:***

* User authenticates and gets to the home screen  (see [home_screen.png](../home_screen.png))

* User clicks the “See cast ballots” button and accesses the “Your cast ballots” screen, where titles, cast ballots count and vote status (vote closed or remaining time) are displayed for the votes for which user has previously cast ballots (see [cast_ballots.png](./cast_ballots.png))

* User clicks on the vote of his/her choice and accesses his ballot’s details: ballot id number, vote description, choice that was cast with the ballot, vote status (vote closed or remaining time)

* If the vote status is “Vote closed”, user can click a :”See vote results” button and see the vote’s results (see [ballot_details_closed_vote.png](./ballot_details_closed_vote.png))

* If the vote is still active, user can click a :”Replace ballot” button and accesses the ballot form (see [ballot_details_active_vote.png](./ballot_details_active_vote.png))
