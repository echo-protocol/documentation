## USE CASE: SUBMIT A NEW VOTE PROPOSAL

***Actors:***

- User

***Scenario:***

* User authenticates and gets to the home screen (see [home_screen.png](../home_screen.png))

* User clicks the “Submit new proposal” button and accesses the proposal submission form (see [proposal_submission_form.png](./proposal_submission_form.png))

* User enters proposal title, proposal description, choices count and choices labels, then clicks the “Submit proposal” button (see [proposal_submission_form_filled.png](./proposal_submission_form_filled.png))

* User sees the confirmation screen: “Success, your proposal has been submitted” with the proposal id number (see [proposal_submission_confirmation.png](./proposal_submission_confirmation.png))
