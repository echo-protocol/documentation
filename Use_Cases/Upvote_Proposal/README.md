## USE CASE: UPVOTE A PROPOSAL

***Actors:***

* User

***Scenario:***

* User authenticates and gets to the home screen (see [home_screen.png](../home_screen.png))

* User clicks the “Proposals” tab and accesses the “Proposals” screen, where all active proposals titles and upvotes counts are displayed (see [proposals.png](./proposals.png))

* User clicks on the proposal of his/her choice and accesses the proposals’s details: proposal id number, title, description, choices, status (proposal submitted, vote deployed or proposal discarded) and upvotes count (see [proposal_details.png](./proposal_details.png))

* User clicks the “Upvote proposal” button and accesses a screen where proposals title, description and choices are displayed (see [upvote.png](./upvote.png))

* User clicks the “Confirm and upvote proposal” button

* User sees the confirmation screen: “Success, your upvote has been taken into account” (see [upvote_confirmation.png](./upvote_confirmation.png))
