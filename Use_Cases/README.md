# USE CASES REFERENCE TABLE

| ***Use case name***  |  ***Link to use case*** | ***Description*** |
| :-- | :-- | :-- |
| Use case 1 | [Use case 1: Submit New Vote Proposal](./Submit_New_Vote_Proposal/README.md)   | User submits a new vote proposal
| Use case 2 | [Use case 2: See Previously Submitted Proposal](./See_Previously_Submitted_Proposal/README.md)   | User accesses previously submitted prooposals
| Use case 3 | [Use case 3: Up-vote a Proposal](./Upvote_Proposal/README.md)   | User up-votes an active proposal
| Use case 4 | [Use case 4: Cast a Ballot for a Vote](./Cast_Ballot/README.md)   | User casts a ballot for an active vote
| Use case 5 | [Use case 5: See Previously Cast Ballots](./See_Previously_Cast_Ballots/README.md)   | User accesses previously cast ballots and can see vote's results (in the case of an already closed vote) or replace a previously cast ballot (in the case of an active vote) 


