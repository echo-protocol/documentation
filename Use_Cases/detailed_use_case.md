## DETAILED USE CASE ECHO VOTING PROTOCOL

***Actors:***
* User
* Authentication Service (AS)
* Agora Backend Service (ABS)
* Ballot Box Smart Contract (BBSC)

***Scenario:***

****Stage 0: Registration - Authentication****

* User registers credentials on ABS
* (ABS passes user’s credentials to AS) (-> AS confirms (or not) user’s authentication to ABS)
* ABS confirms (or not) authentication to user
* (If authentication successful, ABS generates a secret-key for the user, passes it to user and saves its hash along with username and password)

****Stage 1: Proposal Submissions and Upvotes****

* *****OPTIONAL:***** User submits vote proposal to ABS ( proposal = { question, choices, vote start date, vote end date })

* User gets the list of active proposals with their status (proposal, upvotes count)

* *****OPTIONAL:***** User upvotes a proposal


****Stage 2: Vote Deployment****

*****Once a proposal has reached a certain number of upvotes*****

* Delegates generate a unique public key for the vote and send it to ABS (delegates could be human users or smart contracts?)

* ABS deploys a BBSC which implements the vote described in the proposal (BBSC is deployed with the proposal, the public key and the list of authorised user secret-keys hashes)

****Stage 3: Vote****

* User gets the list of active votes with their status (proposal, choices, public key, vote start date, vote end date, votes count, vote result)
* User generates a one-time unique Ethereum account (public/private key-pair) and registers the public key in the BBSC he wants to vote on (using his secret-key)

*****The following sequence can be repeated any amount of times, each new ballot cast replaces the previous one that was cast by the same user*****

* User sends an encrypted ballot to BBSC (along with some cryptographic proofs and the previously cast ballot if applicable) in the form of a blockchain transaction originating from the registered Ethereum account
* If applicable: BBSC verifies that the hash of the previously cast ballot matches the one saved
* If verification is successful:
	* BBSC verifies the proofs
	*  If verification is successful:
		* BBSC de-aggregates previously cast ballot
		* BBSC aggregates the new ballot
		* BBSC sends a receipt to user (cryptographic proof that his new ballot has been cast and previous one has been de-aggregated)
		* BBSC saves a hash of the new ballot alongside user’s public Ethereum address
    * Else: user gets a receipt saying that ballot was not cast

****Stage 4: Tally****

*****Once the vote end date has been reached*****

* Delegates generate the private key that will be used to decrypt the vote and send it to BBSC
* BBSC decrypts the aggregated vote result
* BBSC performs the tally and publishes the vote result
* User gets the list of active votes with their status (proposal, choices, public key, vote start date, vote end date, votes count, vote result)






