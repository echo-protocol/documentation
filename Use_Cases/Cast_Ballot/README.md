## USE CASE: CAST A BALLOT FOR A VOTE

***Actors:***

* User

***Scenario:***

* User authenticates and gets to the home screen (see [home_screen.png](../home_screen.png))

* User clicks the “Votes” tab and accesses the “Votes” screen, where all active votes titles, cast ballots count and remaining time are displayed (see [votes.png](./votes.png))

* User clicks on the vote of his/her choice and accesses the vote’s details: vote id number, title, description, choices, status (active vote or closed vote), remaining time and ballots cast count (see [vote_details.png](./vote_details.png))

* User clicks the “Go to ballot form” button and accesses a screen where vote’s title and description are displayed, as well as vote’s choices in the form of tick boxes (see [ballot_form.png](./ballot_form.png))

* User ticks the choice he wishes to vote for (see [ballot_form_filled.png](./ballot_form_filled.png))

* User clicks the “cast ballot button”

* User accesses a screen where the vote’s description is displayed, along with the choice that was ticked by user (see [ballot.png](./ballot.png))

* User clicks the “Confirm and cast ballot” button

* User sees a confirmation screen “Confirmation, your ballot has been successfully cast” with the ballot id number and a link to access ballot’s details (see [ballot_cast_confirmation.png](./ballot_cast_confirmation.png))
